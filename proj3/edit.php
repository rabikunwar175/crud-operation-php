<?php
        include ('connection.php');
        if (isset($_POST['submit'])){
            $id=$_POST['id'];
            $name= $_POST['name'];
            $email= $_POST['email'];
            $phoneno= $_POST['phoneno'];
            $address=$_POST['address'];
            $dob= $_POST['dob'];
            $courseid=$_POST['courseid'];
            if($_POST['name']&&$_POST['email']&&$_POST['address']&&$_POST['dob']&&$_POST['courseid']!=''){
                try{
                $edit= $conn->prepare("UPDATE student SET name= :name,email= :email,phoneno= :phoneno,address= :address,dob= :dob,courseid= :courseid WHERE id= :id"); 
                $edit->bindParam(':id',$id);
                $edit->bindParam(':name',$name);
                $edit->bindParam(':email', $email);
                $edit->bindParam(':phoneno', $phoneno);
                $edit->bindParam(':address', $address);
                $edit->bindParam(':dob', $dob);
                $edit->bindParam(':courseid', $courseid);
                $edit->execute();
                echo"Inserted successfully";
                if($edit){
                header('Location:index.php');
            }
            }
            catch (PDOException $e){
                echo $e->getMessage();
            }
        }
            else{
                $msg = "PLEASE FILL ALL FIELDS";
                echo "<script type='text/javascript'>alert('$msg');</script>";
            }
           
        }
        $id= 0;
        $name="";
        $email="";
        $phoneno=0;
        $address="";
        $dob="";
        $courseid=0;
        if(isset($_GET['id'])){
            $id=$_GET['id'];
            $insert=$conn->prepare('SELECT * FROM student where id= :id');
            $insert->execute(array(':id'=>$id));
            $row=$insert->fetch();
            $id=$row['id'];
            $name=$row['name'];
            $email=$row['email'];
            $phoneno =$row['phoneno'];
            $address=$row['address'];
            $dob=$row['dob'];
            $courseid=$row['courseid'];
        }
?>
<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>EDIT</title>
</head>

<body>
    <div class="container">
        <h1 style="text-align:center;color:green">STUDENT MANAGEMENT SYSTEM</h1>

        <div class="row">
            <div class="col-md-2">
                <nav class="nav-sidebar">
                    <ul class="nav">
                        <div class="section5">
                            <li class="active"><a href="#">Home</a></li><br>
                            <li><a href="#">Student</a></li><br>
                            <li><a href="#">Department</a></li><br>
                            <li><a href="#">Course</a></li><br>
                            <li><a href="#">Teacher</a></li>
                        </div>
                    </ul>
                </nav>
            </div>
            <div class="col=md-10">
                <div class="section1">
                    <form method="post">
                        <h3>Edit Information</h3>
                        <div class="form-group">
                            <input type="hidden" name="id" value="<?=$id;?>">
                            <label for="student_name">Student Name:</label>
                            <input type="text" name="name" value="<?=$name;?>" class="form-control" placeholder="Enter student name"
                                style="width: 350px;">
                            <label for="student_name">Email:</label>
                            <input type="text" name="email" value="<?=$email;?>" class="form-control" placeholder="examp&eaa.com"
                                style="width: 350px;">
                            <label for="student_name">Phone No:</label>
                            <input type="text" name="phoneno" value="<?=$phoneno;?>" class="form-control" placeholder="Enter phone no"
                                style="width: 350px;">
                            <label for="student_name">Address:</label>
                            <input type="text" name="address" value="<?=$address;?>" class="form-control" placeholder="Enter Address"
                                style="width: 350px;">
                            <label for="DOB">DOB:</label>
                            <input type="text" name="dob" value="<?=$dob;?>" class="form-control" placeholder="Enter date of birth"
                                style="width: 350px;">
                            <label for="courseid">Course ID:</label>
                            <input type="text" name="courseid" value="<?=$courseid;?>" class="form-control" placeholder=""
                                style="width: 350px;">
                            <button type="submit" name="submit" class="btn btn-primary">Insert</button>
                        </div>
                </div>

            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="footer">
            <h2 style="text-align:center;color:white;">@COPYRIGHT</h2>
        </div>
    </div>

</body>

</html>