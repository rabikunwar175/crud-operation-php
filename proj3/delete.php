<?php
include ('connection.php');
if (isset($_GET['id'])){
    $id=$_GET['id'];
    try{
        $delete=$conn->prepare("DELETE FROM student WHERE id=?");
        $delete->execute(array($id));
        header('Location:index.php');
    }
    catch(PDOException $e){
        echo $e->message();

    }
}
?>