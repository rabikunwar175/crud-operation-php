<?php
try {
    $conn = new PDO('mysql:host=localhost;dbname=school;charset=utf8', 'root', '');
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $conn;
  
}
catch (PDOException $e)
    {
       
    echo "Error"."<br>". $e->getMessage();
    }
?>