<?php
include ('connection.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>add</title>
</head>

<body>
    <div class="section3">
        <h1>STUDENT LIST</h1>
        <a href="add.php">
            <h1>Add New Record</h1>
        </a>
        <div class="container">
            <table class="table table-striped ">
                <thead class="thead-light">
                    <tr>
                        <th>id</th>
                        <th>Name</th>
                        <th>email</th>
                        <th>phoneno</th>
                        <th>address</th>
                        <th>dob</th>
                        <th>courseid</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <?php
            $stmt=$conn->prepare("SELECT * from student ORDER BY id ASC");
            $stmt->execute();
            $results= $stmt->fetchAll();
            foreach($results as $row){
            ?>
                <tr class="primary">
                    <td>
                        <?=$row['id'];?>
                    </td>
                    <td>
                        <?=$row['name'];?>
                    </td>
                    <td>
                        <?=$row['email'];?>
                    </td>
                    <td>
                        <?=$row['phoneno'];?>
                    </td>
                    <td>
                        <?=$row['address'];?>
                    </td>
                    <td>
                        <?=$row['dob'];?>
                    </td>
                    <td>
                        <?=$row['courseid'];?>
                    </td>
                    <td>
                        <a href="edit.php?id=<?=$row['id'];?>">Edit</a>
                        <a href="delete.php?id=<?=$row['id'];?>">Delete</a>

                </tr>
                <?php
               }
               ?>
            </table>
        </div>
    </div>
</body>

</html>