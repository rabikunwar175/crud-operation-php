<!DOCTYPE html>
<html>

<head>

    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">

</head>

<body>
    <div class="container-fluid">
        <h1 style="text-align:center;color:green;background-color:pink;">STUDENT MANAGEMENT SYSTEM</h1>

        <div class="row">
            <div class="col-md-2">
                <nav class="nav-sidebar">
                    <ul class="nav">
                        <div class="section5">
                            <li class="active"><a href="#">Home</a></li>
                            <li><a href="index.php">Student</a></li>
                            <li><a href="#">Department</a></li>
                            <li><a href="#">Course</a></li>
                            <li><a href="#">Teacher</a>
                        </div>
                    </ul>
                </nav>
            </div>
            <div class="col-md-10">
                <div class="section2">
                    <h3>Add Student Information</h3>
                    <form method="post">
                        <div class="form-group">
                            <label for="student_name">Student Name:</label>
                            <input type="text" name="name" class="form-control" placeholder="Enter Student Name" style="width: 350px;">
                            <label for="student_name">Email:</label>
                            <input type="text" name="email" class="form-control" placeholder="examp&eaa.com" style="width: 350px;">
                            <label for="student_name">Phone No:</label>
                            <input type="text" name="phoneno" class="form-control" placeholder="Enter phone no" style="width: 350px;">
                            <label for="student_name">Address:</label>
                            <input type="text" name="address" class="form-control" placeholder="Enter Address" style="width: 350px;">
                            <label for="start">Date Of Birth:</label>
                            <input type="date" name="dob" class="form-control" placeholder="Enter date of birth" style="width: 350px;">
                            <label for="courseid">Course ID:</label>
                            <input type="text" name="courseid" class="form-control" placeholder="" style="width: 350px;">
                            <button type="submit" name="submit" class="btn btn-primary">Insert</button>

                        </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="footer">
                <h2 style="text-align:center;color:white;">@COPYRIGHT</h2>
            </div>
        </div>
</body>

</html>